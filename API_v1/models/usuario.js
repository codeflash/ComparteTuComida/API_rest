/**
 * Created by brian on 16/05/2017.
 */
const usuarioSchema = new Schema({
    nombre: { type: "string", required: true, validate: "isAlpha"},
    correo: { type: "string", required: true, validate: "isEmail"},
    celular: { type: "string", required: true, validate: "isMobilePhone"},
    contrasenia: { type: "string",required: true},
    tipo_usuario: { type: "string", default:"com"},
    ubicacion: { type: "geoPoint", optional:true}, //evaluar
    createdOn: { type: 'datetime', default: gstore.defaultValues.NOW},
    modifiedOn: { type: 'datetime' }
});