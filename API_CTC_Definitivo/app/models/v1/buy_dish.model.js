var mongoose = require('mongoose');
 
var buy_dishSchema = mongoose.Schema({

        quantity: { type: Number, required: true },
        user_score: { type: Number, required: true},  // DESDE EL COCINERO
        dish_score: { type: Number, required: true},  // DESDE EL COMESAL
        chef_score: { type: Number, required: true},  // DESDE EL COMESAL
        user_comment: { type: String, required: true}, // DESDE EL COCINERO
        dish_comment: { type: String, required: true}, // DESDE EL COMESAL
        chef_comment: { type: String, required: true}, // DESDE EL COMESAL
        state_buy: { type: Number}, // AUN NO LO HAS COMPRADO
        //1: compra iniciada
        //2: compra realizada
        //3: compra valorada
        state_review: { type: Number},
        //1: valorado x chef
        //2: valorado x usuario
        //3: valorado x los 2
        createdOn: { type: Date, default: Date.NOW},
        modifiedOn: { type: Date,default: Date.NOW },
        closedOn: {type: Date}, // CUANDO EL ESTADO CAMBIO A
        deletedOn: {type: Date},
        id_buyer:   { type: [] },
        id_dish:    { type: String  },

});

module.exports = mongoose.model('buy_dish', buy_dishSchema);