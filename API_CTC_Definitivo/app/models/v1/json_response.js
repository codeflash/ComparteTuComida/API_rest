/**
 * Created by brian on 2/06/2017.
 */

var json_response = function () {

    var content,meta,errors = [];

    return{

        data_response: function (data) {
            content = data
        },

        meta_response: function (data) {
            meta = data
        },

        errors_response: function (data) {
            errors = data
        },


        json_parse: function () {
            return {
                meta: meta,
                data: content,
                errors: errors

            }
        }


    }


};