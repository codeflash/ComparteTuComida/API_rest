/**
 * Created by brian on 16/05/2017.
 */
var validator = require('validator');
var mongoose = require('mongoose');
//var bcrypt = require('bcrypt'); // encriptacion


var userSchema = mongoose.Schema({

    _id: { type: "String", required:true, unique:true},
    name: { type: "String", required: true },
    email: { type: "String", required: true,lowercase: true,
        unique: true},
    password: { type: "String",required: true},
    phone: { type: "String", required: true},
    user_type: { type: "String",  enum: ['diner', 'chef'], default: 'diner'},
    location: {
        type: [Number],
        index: '2d'
    }, //evaluar
    createdOn:  { type: 'Date', default: Date.NOW},
    modifiedOn: { type: 'Date' },
    deletedOn:  { type: 'Date' },
    id_dishes:  {    type: []    },
    id_followers: { type: []    },
    id_buydishes: { type: []    },

});
/*
userSchema.pre('save', function (next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, function(err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

// Create method to compare password input to password saved in database
userSchema.methods.comparePassword = function(pw, cb) {
    bcrypt.compare(pw, this.password, function(err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};
*/

module.exports = mongoose.model('user', userSchema);