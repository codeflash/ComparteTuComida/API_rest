/**
 * Created by brian on 20/05/2017.
 */
var mongoose = require('mongoose');
// ver videos de nodejs CODIGOFACILITO

var dishSchema = mongoose.Schema({
//eliminar data

  // todo lo puse dentro de data para no tener que formatear la respuesta final
        name: { type: String, require:true},
        description: { type: String, require:true},
        photos:{ type: []},
        ingredients: { type: []},
        unitary_cost: { type: Number},
        units: { type: Number, require:true},
        location: {
            type: [Number],
            index: '2d'
        },
        hour_delivery: { type: Date},
        hour_end: { type: Date},
        category: { type: String},
        puntuation: {type: Number, default: 0},
        state: { type: Boolean},
        createdOn: { type: Date, default: Date.now()},
        modifiedOn: { type: Date, default: Date.now()},
        deletedOn: {type: Date },
        //un estado de eliminacion
        username_author: { type: String }


});

module.exports = mongoose.model('dish', dishSchema);