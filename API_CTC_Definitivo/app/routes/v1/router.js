/**
 * Created by brian on 19/05/2017.
 */
var user = require('../../models/v1/user.model');
var userC = require('../../controllers/v1/user.controller');
var dishC = require('../../controllers/v1/dish.controller');
var buy_dish = require('../../models/v1/buy_dish.model');
var buy_dishC = require('../../controllers/v1/buy_dish.controller');

// ROUTES FOR OUR API
// =============================================================================

// create our router
var express    = require('express');
var router = express.Router();

// User routes
router.get('/user', userC.findAll);
router.get('/user/:user_id',userC.findOne);
router.post('/user', userC.create);
router.put('/user/:user_id', userC.updateUser);
router.delete('/user/:user_id', userC.deleteUser);


// Dishes get routes
router.get('/dish', dishC.findAll);
router.get('/loc', dishC.findAllByNear);     //pruebas de location con el dish
router.get('/dish/:dish_id', dishC.findOne);
//router.get('/dish?lng=:lngDish&lat=:latDish', dishC.findAllByNear);
//router.get('/dish?name=:name', dishC.findAllByName);
//router.get('/dish?category=:DishCategory', dishC.findAllByType);
//router.get('/user/:id/dish/:id', dishC.findAllFromUser);
router.post('/dish', dishC.create);
router.put('/dish/:dish_id', dishC.update); // CAMBIA TODO
router.patch('/dish/:dish_id', dishC.updateSome); 
router.delete('/dish/:dish_id', dishC.delete);
//Drishes - buyDishes routes
router.get('/dish/:dish_id/buy', buy_dishC.findAllByDish), //muestra las compras por plato
router.get('/dish/:dish_id/reviews',   buy_dishC.findDishReviews), //muestra los reviews

    //override para formatear las cabeceras
    //javascript croosside origin


// buy_dish routes

router.get('/buy_dish', buy_dishC.findAll);
router.get('/buy_dish/:buy_dish_id',buy_dishC.findOne);
router.post('/buy_dish/:dish_id', buy_dishC.create);
router.put('/buy_dish/:buy_dish_id', buy_dishC.update);
router.delete('/buy_dish/:buy_dish_id', buy_dishC.delete);


module.exports = router;