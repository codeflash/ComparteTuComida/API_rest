/**
 * Created by brian on 20/05/2017.
 */
var Dish = require('../../models/v1/dish.model');
var mongoose = require('mongoose');
var val = require('../../../vali');
var jsonRes = require('../../models/v1/json_response');

module.exports = {

    //encuentra todos los platos almacenados
    findAll: function(req,res) {

        //consulta construida para invocar el latlng y buscar cerca
        var lat, lng; //-16.42546, -71.51352 para pruebas

        lat = -16.42546; // req.query.lat;
        lng = -71.51352;// req.query.lng;
        console.log("lat :" + lat + " lng: " + lng);
        var distance = 500; //40 / 6371;  // aun no se a cuanto equivale (metros radiales)??


        if (req.query.category != null) {
            console.log(req.query.category + " categoria");
            Dish.find({
                'location': {
                    $near: [
                        lat,
                        lng
                    ],
                    $maxDistance: distance
                }, 'category': req.query.category
            }, function (err, dishes) {
                if (err) {
                    console.log(err);
                    throw err;
                }

                if (!dishes) {
                    res.json({});
                } else {

                    res.json(                        jsonRes.data_response = dishes,
                        jsonRes.meta_response={ status:200 },
                        jsonRes.errors_response={ error: 404 },

                        jsonRes.json_parse);
                }

            });

            /*           Dish.find({category:req.query.category},function (err, dishes) {
             if (err)
             res.send(err);

             res.json(dishes);

             })*/


        }
        else if (req.query.name != null) {
            console.log(req.query.name + " nombre ");
            Dish.find({
                'location': {
                    $near: [
                        lat,
                        lng
                    ],
                    $maxDistance: distance
                }, 'name': new RegExp(".*" + req.query.name.toLowerCase() + ".*"    )

            }, function (err, dishes) {
                if (err) {
                    console.log(err);
                    throw err;
                }

                if (!dishes) {
                    res.json({});
                } else {

                    res.json(                        jsonRes.data_response = dishes,
                        jsonRes.meta_response={ status:200 },
                        jsonRes.errors_response={ error: 404 },

                        jsonRes.json_parse);
                }

            });

            /*Dish.find({name:req.query.name},function (err, dishes) {
             if (err)
             res.send(err);

             res.json(dishes);

             })*/
        }
        else {

            Dish.find({
                    'location': {
                    $near: [
                        lat,
                        lng
                    ],
                    $maxDistance: distance
                }

            }, function (err, dishes) {
                if (err) {
                    console.log(err);
                    throw err;
                }

                if (!dishes) {
                    res.json({});
                } else {

                    res.json(
                        jsonRes.data_response = dishes,
                        jsonRes.meta_response={ status:200 },
                        jsonRes.errors_response={ error: 404 },

                        jsonRes.json_parse
                    );
                }

            });
            /*Dish.find(function (err, dishes) {
             if (err)
             res.send(err);

             res.json(dishes);

             })
             }*/

        }
    },

    //encuentra un plato por el id del plato
    findOne: function(req,res){
        Dish.findById(req.params.dish_id ,function (err, dish) {
            console.log(req.baseUrl+req.url);
            if (err)
                res.send(err);

            res.json({
                dish:dish

            });

        })
    },

    //encuentra los platos cercanos  ----  funcion de prueba de la busqueda
    findAllByNear: function(req,res){
        var lat, lng;
        lat = req.query.lat;
        lng = req.query.lng;
        console.log("lat :"+lat+" lng: "+lng);
        var distance = 40 / 6371;

        var query = Dish.find({'location': {
            $near: [
                lat,
                lng
            ],
            $maxDistance: distance
        }
        });

        query.exec(function (err, dishes) {
            if (err) {
                console.log(err);
                throw err;
            }

            if (!dishes) {
                res.json({});
            } else {

                res.json(dishes);
            }

        });
    },

    //crea un plato por metodo post
    create: function(req,res){
        var photosArray = [], ingredientsArray = [];
        //se cargaran en arrays primero las fotos y los ingredientes.
        photosArray.push(req.body.photos);
        ingredientsArray.push(req.body.ingredients);

        var dish = new Dish({
                name :          req.body.name.toLowerCase(),
                description:    req.body.description,
                photos:         photosArray,
                ingredients:    ingredientsArray,
                unitary_cost:   req.body.unitary_cost,
                units:          req.body.units,
                location:       [req.body.lat, req.body.lng], //se guarda de frente el lat y lng en nuestra propiedad
                hour_delivery:  req.body.hour_delivery,
                hour_end:       req.body.hour_end,
                state:          req.body.state,
                category:       req.body.category,
                // puntuation: 0  por defecto se creará en 0
                username_author:      "brian.pareja" //por session
        });

        
        dish.save(function(err) {
            if (err)
                res.send(err);
            else
                res.json(

                    { message: 'Dish created!' }

                    );
        });

        
       

       
    },

    update: function (req,res) {
        Dish.findById(req.params.dish_id ,function (err, dish) {
            if (err)
                res.send(err);
            else
                var photosArray = [], ingredientsArray = [];
            //se cargaran en arrays primero las fotos y los ingredientes.
                photosArray.push(req.body.photos);
                ingredientsArray.push(req.body.ingredients);

                dish.name = req.body.name,
                dish.description= req.body.description,
                        //se guarda el array lleno
                dish.photos= photosArray,
                dish.ingredients= ingredientsArray,
                dish.unitary_cost= req.body.unitary_cost,
                dish.units= req.body.units,
                dish.location= [req.body.lat, req.body.lng], //se guarda del author
                dish.hour_delivery= req.body.hour_delivery,
                dish.hour_end= req.body.hour_end,
                dish.state= req.body.state,
                dish.category= req.body.category,
                dish.modifiedOn = Date.now(),
                dish.save();
                        dish.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'Dish updated!' });
                });

            })},

    updateSome: function (req,res) {
        Dish.findByIdAndUpdate(req.params.dish_id, req.body ,function (err, users) {
                if (err)
                    res.send(err);
                else
                    res.json({ message: 'Successfully update' });
                })
    },

    delete: function(req,res){
        Dish.remove({
            _id: req.params.dish_id
        }, function(err, dish) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    }


};


/*
    falta:

    - ACTIALIZACION DE ALGUN VALOR
    -
    -
    -
    -
    
*/