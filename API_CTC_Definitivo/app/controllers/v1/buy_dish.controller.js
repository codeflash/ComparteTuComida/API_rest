/**
 * Created by brian on 19/05/2017.
 */
var Buy_dish = require('../../models/v1/buy_dish.model');
var Dish = require('../../models/v1/dish.model');
var val = require('../../../vali');
var mongoose = require('mongoose');

module.exports = {

    findAll: function(req,res){
        Buy_dish.find(function (err, buy_dishes) {
            if (err)
                res.send(err);

            res.json(buy_dishes);

        })
    },

    findOne: function(req,res){
        Buy_dish.findById(req.params.buy_dish_id ,function (err, buy_dishes) {
            if (err)
                res.send(err);

            res.json(buy_dishes);

            

        })
    },

    create: function(req,res){
        var buy_dish = new Buy_dish({
        
                quantity: req.body.quantity,
                user_score: req.body.user_score,
                dish_score: req.body.dish_score,
                chef_score: req.body.chef_score,
                user_comment: req.body.user_comment,
                dish_comment: req.body.dish_comment,
                chef_comment: req.body.chef_comment,
                state: req.body.state
                //location: req.body.
        });

        buy_dish.save(function(err) {

            if (err)
                res.send(err);
            else{

                Dish.findById(req.params.dish_id ,function (err, dish) {
            if (err)
                res.send(err);
            else{
                
                       
                        dish.units= dish.units - req.body.quantity
                        
                        dish.save(function(err) {
            
                    if (err)
                        res.send(err);

            
                });

                }
            })
              
                
            res.json({ message: 'buy_dish created!'+req.body.quantity });
            }



        });

    },

    delete: function(req,res){
        Buy_dish.remove({
            _id: req.params.buy_dish_id
        }, function(err, buy_dish) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    },

    update: function (req,res) {
        Buy_dish.findById(req.params.buy_dish_id ,function (err, buy_dishes) {
            if (err)
                res.send(err);
            else{

                buy_dishes.quantity= req.body.quantity,
                    buy_dishes.user_score= req.body.user_score,
                    buy_dishes.dish_score= req.body.dish_score,
                    buy_dishes.chef_score= req.body.chef_score,
                    buy_dishes.user_comment= req.body.user_comment,
                    buy_dishes.dish_comment= req.body.dish_comment,
                    buy_dishes.chef_comment= req.body.chef_comment,
                    buy_dishes.state= req.body.state
                buy_dishes.save(function(err) {
                    if (err)
                        res.send(err);

                    res.json({ message: 'Dish updated!' });
                });
            }


        })

            //location: req.body.location
        },
        /*Buy_dish.findByIdAndUpdate(req.params.buy_dish_id, req.body, function (err, buy_dishes) {
            if (err)
                res.send(err);
            else
                res.json({ message: 'Successfully update' });

        })*/


    findAllByDish: function (req,res){ //muestra las compras por plato - alternando entre hoy e historico
      dishid = req.params.dish_id;
      if(req.query.ref == "today") {
          console.log("today " + dishid)
          var hoy = Date.now()
          Buy_dish.find({"meta.createdOn": hoy}, function (err, buy_dishes) {
              if (err)
                  res.send(err);

              res.json(buy_dishes);

          })
      }

       else if(req.query.ref == "all"){
          console.log("all "+ dishid)
       }else{}
    },
    findDishReviews: function (req,res){ //muestra solo los reviews del plato

    }

};





/*
    falta:

    - AL CREAR UNA ORDEN DE COMPRA... EDITAR MODELO PLATO (cantidad)
    - MODIFICAR SOLO ALGUNOS CAMPOS( DEL COCINERO, DEL USUARIO)
    - AL MOSTRAR EL BUY DISH, TIENES QUE MOSTRAR EL NOMBRE DEL USUARIO QUE ESTA HACIENDO LA COMPRA
    - ENCUETRAS LAS ORDENES DE COMPRA X PLATO --> DE HOY!!!
    - FUNCION y RUTA --> QUE ME DEVUELVA TODOS LOS COMENTARIOS DEL PLATO Y USUARIO/COCINERO
    * MANEJANDO LOS ESTADOS -  TIENEN QUE ESTAR ACTUALIZADOS TODOS LAS PUNTUACIONES (chef, cocinero, plato)
    
*/