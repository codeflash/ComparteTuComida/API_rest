/**
 * Created by brian on 19/05/2017.
 */
var User = require('../../models/v1/user.model');
var val = require('../../../vali');
var mongoose = require('mongoose');
var validator = require('validator');
//var bcrypt = require('bcrypt'); // encriptacion

module.exports = {

    register: function(req,res){
        if(!req.body.email || !req.body.password) {
            res.json({ success: false, message: 'Please enter email and password.' });
        } else {
            var newUser = new User({
                _id: req.user._id,
                email: req.body.email,
                password: req.body.password,
                name: req.body.name,
                phone: req.body.phone,
                user_type: req.body.user_type
            });

            // Attempt to save the user
            newUser.save(function(err) {
                if (err) {
                    return res.json({ success: false, message: 'That email address already exists.'});
                }
                res.json({ success: true, message: 'Successfully created new user.' });
            });
        }
    },

    findAll: function(req,res){
        User.find(function (err, users) {
            if (err)
                res.send(err);

            res.json(users);

        })
    },

    findOne: function(req,res){
        User.findById(req.params.user_id ,function (err, users) {
            if (err)
                res.send(err);

            res.json(users);

        })
    },



/*
    falta:
    - login
    
*/

    create: function(req,res){
        var user = new User({
            _id: req.body._id,
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            password: req.body.password,
            user_type: req.body.user_type
            //location: req.body.location
        });

       // if (val.valiU(user)) {

        user.save(function(err) {
            //console.log(val.valiU(user))
            if (err)
                res.send(err);
            else
            res.json({ message: 'User created!' });
        });

        //}
       // else
          //  res.json({ message: 'Campos invalidos' });

    },

    deleteUser: function(req,res){
        User.remove({
            _id: req.params.user_id
        }, function(err, user) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    },

    updateUser: function (req,res) {
        User.findByIdAndUpdate(req.params.user_id, req.body ,function (err, users) {
            if (err)
                res.send(err);
            else
                res.json({ message: 'Successfully update' });

        })
    }
};




