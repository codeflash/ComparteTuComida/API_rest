/**
 * Created by brian on 19/05/2017.
 */


var app = require('./app');
var port     = process.env.PORT || 3000; // set our port
var router = require('./app/routes/v1/router');
var mongoose   = require('mongoose');
mongoose.connect('mongodb://192.168.99.100:32770/ctc'); // connect to our database

app.use('/api', router); //pasarle

app.listen(port);
console.log('Магия случается в порту ' + port);
