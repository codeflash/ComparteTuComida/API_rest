/**
 * Created by brian on 18/05/2017.
 */
var express    = require('express');
var router = express.Router();
var user = require('../../controllers/v1/userController');

router.get('/bus', user.findAll);
module.exports = router;
