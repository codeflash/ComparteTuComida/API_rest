
var port = process.env.PORT || 3000;
var router = express.Router();

var user = require('./routes/v1/user.route');

router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

//console.log(router);
//console.log(bus);
app.use('/' , router);
app.use('/api' , user);

app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
});
