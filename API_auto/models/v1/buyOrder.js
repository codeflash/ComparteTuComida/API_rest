/**
 * Created by brian on 16/05/2017.
 */
const gstore = require('gstore-node');
const Schema = gstore.Schema;

const orden_compraSchema = new Schema({
    cantidad: { type: "int"},
    puntaje_usuario: { type: "int"},
    puntaje_plato: { type: "int"},
    puntaje_cocinero: { type: "int"},
    comentario_usuario: { type: "string"},
    comentario_plato: { type: "string"},
    comentario_cocinero: { type: "string"},
    estado: { type: "boolean"},
    createdOn: { type: 'datetime', default: gstore.defaultValues.NOW },
    modifiedOn: { type: 'datetime' }
});


const User = gstore.model('User', orden_compraSchema);
