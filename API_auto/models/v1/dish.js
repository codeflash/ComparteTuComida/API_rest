/**
 * Created by brian on 16/05/2017.
 */
const gstore = require('gstore-node');
const Schema = gstore.Schema;

const platoSchema = new Schema({

    nombre: { type: "string", validate: ' isAlphanumeric'},
    descripcion: { type: "string", validate: ' isAlphanumeric'},
    ingredientes: { type: "array"},
    costo_unitario: { type: "double"},
    unidades: { type: "int"},
    lugar_entrega: { type: "geoPoint"},
    hora_entrega: { type: "datetime"},
    hora_fin: { type: "datetime"},
    estado: { type: "boolean"},
    createdOn: { type: 'datetime', default: gstore.defaultValues.NOW},
    modifiedOn: { type: 'datetime' }

});


const User = gstore.model('User', platoSchema);
