/**
 * Created by brian on 18/05/2017.
 */
'use strict'
var routes = require('./routes/v1/route');
var app = require('./app');
var  mongoose = require('mongoose'),
     port = process.env.PORT || 3000;
   //  User = require('./models/v1/user.model');

mongoose.connect("mongodb://localhost:27017/ctc",(err,res) => {
    if(err){
        throw err;
    }
    else{
        console.log("Conexion a mongo correcta");
        app.listen(port, function(){
            console.log("Api funcionando en puerto "+port)
        });
    }
});


