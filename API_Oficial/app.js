var express = require('express'),
    app = express(),
    router = require('./routes/v1/route')
    bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/router', router);


module.exports = app;
