/**
 * Created by brian on 16/05/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const dishSchema = new Schema({

    name: { type: "String"},
    description: { type: "String"},
    ingredients: { type: "[]"},
    cost_unitary: { type: "Number"},
    units: { type: "Number"},
    place_delivery: { 
        type: [Number],
        index: '2d'
    },
    hour_delivery: { type: "Date"},
    hour_end: { type: "Date"},
    state: { type: "Boolean"},
    createdOn: { type: 'Date', default: gstore.defaultValues.NOW},
    modifiedOn: { type: 'Date' }

});


const User = mongoose.model('Dish', platoSchema);
