/**
 * Created by brian on 16/05/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const orden_compraSchema = new Schema({
    cantidad: { type: "Number"},
    puntaje_usuario: { type: "Number"},
    puntaje_plato: { type: "Number"},
    puntaje_cocinero: { type: "Number"},
    comentario_usuario: { type: "String"},
    comentario_plato: { type: "String"},
    comentario_cocinero: { type: "String"},
    estado: { type: "Boolean"},
    createdOn: { type: 'Date', default: gstore.defaultValues.NOW },
    modifiedOn: { type: 'Date' }
});


const User = gstore.model('User', orden_compraSchema);
