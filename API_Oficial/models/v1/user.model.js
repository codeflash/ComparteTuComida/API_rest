/**
 * Created by brian on 16/05/2017.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


    name: { type: "String", required: true },
    email: { type: "String", required: true},
    phone: { type: "String", required: true},
    pass: { type: "String",required: true},
    user_type: { type: "String", default:"com"},
    location: {
    	type: [Number],
    	index: '2d'
    }, //evaluar
    createdOn: { type: 'Date', default: Date.NOW},
    modifiedOn: { type: 'Date' }


module.exports = mongoose.model('User', userSchema);